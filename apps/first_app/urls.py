from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^dashboard$', views.dashboard),
    url(r'^wish_list/create$', views.create),
    url(r'^createnow$', views.createnow),
    url(r'^show/(?P<item_id>\d+)$', views.show),
    url(r'^wish/(?P<item_id>\d+)$', views.wish),
    url(r'^logout$', views.logout),
    url(r'^delete/(?P<id>\d+)$', views.delete),
    url(r'^remove/(?P<id>\d+)$', views.remove_from_wishlist)

]
