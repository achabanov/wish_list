from __future__ import unicode_literals
from django.db import models
import re
import bcrypt
from datetime import date, datetime
from time import strptime
n_RegEx = re.compile(r'^[A-Za-z ]+$')


# Create your models here.
class userManager(models.Manager):
    def validate (self, postData):
# checks for all errors:
        errors = []
        if len(postData['name']) < 3:
            errors.append("Name needs to be more than 2 letters.")
        if len(User.objects.filter(username=postData['username'])) > 0:
            errors.append("Username already exists.")
        if len(postData['username']) < 3:
            errors.append("Name needs to be more than 2 letters.")
        if not n_RegEx.match(postData['name']):
            errors.append("Name needs to be only letters.")
        if len(postData['password']) < 8:
            errors.append("Password needs to be more than 7 letters.")
        if postData['password'] != postData['confirm_password']:
            errors.append("Your passwords do not match.")
        if str(date.today()) < str(postData['doh']):
            errors.append("Please input a valid date, the date of hire cannot be in the future.")
# creates new user - errors:
        if len(errors) == 0:
            newuser = User.objects.create(name=postData['name'], username=postData['username'], password=bcrypt.hashpw(postData['password'].encode(), bcrypt.gensalt()), doh= postData['doh'])
            return (True, newuser)
        else: # maybe a system error?
            return (False, errors)


    def login(self, postData):
        errors = []
        if 'username' in postData and 'password' in postData:
            try:
                user = User.objects.get(username = postData['username'])
            except User.DoesNotExist:
                errors.append("Please try logging in again.")
                return (False, errors)

        if bcrypt.checkpw(postData['password'].encode(), user.password.encode()):
            return (True, user)
        else:
            errors.append("Please try again.")
            return (False, errors)

class User(models.Model):
    name = models.CharField(max_length=45)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    doh= models.DateField(blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    objects = userManager()

class itemControl(models.Manager):
    def itemval(self, postData, id):
        errors=[]
        if len(postData['description']) < 1:
            errors.append("Description field can not be empty.")
        if len(postData['description']) < 4:
            errors.append("Description field must be more than 3 characters.")
        if len(errors) == 0:
            newwishitem = Item.objects.create(description=postData['description'], creator= User.objects.get(id=id))
            return (True, newwishitem)
        else:
            return (False, errors)
    def wish(self, id, item_id):
        if len(Item.objects.filter(id=item_id).filter(wish__id=id)) > 0:
            return {'errors': 'You already wished for this.'}
        else:
            wisher = User.objects.get(id=id)
            newwishitem = self.get(id= item_id)
            newwishitem.join.add(wisher)
            return {}


class Item(models.Model):
    description = models.CharField(max_length=255)
    creator = models.ForeignKey(User, related_name= "wish_add")
    wish = models.ManyToManyField(User, related_name="wisher") # holds on to instances of User
    created_at = models.DateField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)
    objects = itemControl()
