from django.shortcuts import render, redirect
from django.contrib import messages
from django.db.models import Count
from .models import User, Item
from django.db.models import Q
# Create your views here.

def index(request):
    return render(request, 'first_app/index.html')


def register(request):
    if request.method == 'GET':
        return redirect('/')
    newuser = User.objects.validate(request.POST)
    # print newuser
    if newuser[0] == False:
        for each in newuser[1]:
            messages.error(request, each)
        return redirect('/')


    if newuser[0] == True:
        messages.success(request, 'Excellent.')
        request.session['id'] = newuser[1].id
        return redirect('/dashboard')


def login(request):
    if 'id' in request.session:
        return redirect('/dashboard')
    if request.method == 'GET':
        return redirect('/')
    else:
        user = User.objects.login(request.POST)
        if user[0] == False:
            for each in user[1]:
                messages.add_message(request, messages.INFO, each)
            return redirect('/')
        if user[0] == True:
            messages.add_message(request, messages.INFO,'You are logged in.')
            request.session['id'] = user[1].id
            return redirect('/dashboard')


def dashboard(request):
    if 'id' not in request.session:
        return redirect ('/')
    context = {
        "user": User.objects.get(id=request.session['id']),
        "items" : Item.objects.all(),
        "others": Item.objects.all().exclude(wish__id=request.session['id']),
    }
    return render(request, 'first_app/dashboard.html', context)


def create(request):
    if 'id' not in request.session:
        return redirect('/')
    else:
        context= {
            "user":User.objects.get(id=request.session['id']),
        }
        return render(request, 'first_app/create.html', context)


def createnow(request):
    if request.method != 'POST':
        return redirect ('/create')
    newwish= Item.objects.itemval(request.POST, request.session['id'])
    if newwish[0] == True:
        return redirect('/dashboard')
    else:
        for message in newwish[1]:
            messages.error(request, message)
        return redirect('/wish_list/create')


def show(request, item_id):
    try:
        item = Item.objects.get(id=item_id)
        #x = Q(creator=request.session['id'])
        #y = Q(wisher__id=id)
        all_users = User.objects.filter(Q(wisher=item))
        print()
    except Item.DoesNotExist:
        messages.info(request, "Item not found.")
        return redirect('/dashboard')
    context = {
        "item": item,
        "user": User.objects.get(id=request.session['id']),
        "all_users": all_users,
    }
    print(context)
    return render(request, 'first_app/details.html', context)


def wish(request, item_id):
    item = Item.objects.get(id=item_id)
    user = User.objects.get(id=request.session["id"])
    item.wish.add(user)
    return redirect('/dashboard')


def delete(request, id):
    try:
        target= Item.objects.get(id=id)
    except Item.DoesNotExist:
        messages.info(request,"Item does not exist.")
        return redirect('/dashboard')
    target.delete()
    return redirect('/dashboard')

def remove_from_wishlist(request, id):
    try:
        item = Item.objects.get(id=id)
    except Item.DoesNotExist:
        messages.info(request,"Item does not exist.")
        return redirect('/dashboard')
    wisher_id = User.objects.get(id=request.session['id'])
    print(wisher_id.id)
    item.wish.remove(wisher_id)
    return redirect('/dashboard')


def logout(request):
    if 'id' not in request.session:
        return redirect('/')
    del request.session['id']
    return redirect('/')










